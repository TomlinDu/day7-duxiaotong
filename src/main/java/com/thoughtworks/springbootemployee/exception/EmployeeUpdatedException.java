package com.thoughtworks.springbootemployee.exception;

public class EmployeeUpdatedException extends RuntimeException {
    public EmployeeUpdatedException(String message) {
        super(message);
    }
}

