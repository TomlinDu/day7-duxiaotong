package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entities.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private static final List<Employee> employeeList = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public Employee addEmployee(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employeeList.add(employee);
        return employee;
    }

    public Employee findById(Long id) {
        return employeeList.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst().orElse(null);
    }

    public List<Employee> findAll() {
        return employeeList;
    }

    public List<Employee> findByGender(String gender) {
        return employeeList.stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    public List<Employee> findEmployeeByPageAndSize(int page, int size) {
        return employeeList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee update(Long id, Employee employeeMessage) {
        Employee findEmployee = employeeList.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst().orElse(null);
        assert findEmployee != null;
        findEmployee.setAge(employeeMessage.getAge());
        if (Objects.nonNull(employeeMessage.getSalary()))
            findEmployee.setSalary(employeeMessage.getSalary());
        if (Objects.nonNull(employeeMessage.getCompanyId()))
            findEmployee.setCompanyId((Long) employeeMessage.getCompanyId());
        return findEmployee;
    }


    public void deleteEmployeeById(Long id) {
        employeeList.removeIf(employee -> Objects.equals(employee.getId(), id));
    }

    public List<Employee> getEmployeesByCompanyId(long id) {
        return employeeList.stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), id))
                .collect(Collectors.toList());
    }

    public void clearAll() {
        employeeList.clear();
    }

    public void updateEmployeeStatusByCompanyId(long id) {
        employeeList.forEach(employee -> {
            if (Objects.equals(employee.getCompanyId(), id)) {
                employee.setStatus(false);
                employee.setCompanyId(null);
            }
        });
    }
}
