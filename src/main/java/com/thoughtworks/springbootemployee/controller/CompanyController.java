package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entities.Company;
import com.thoughtworks.springbootemployee.entities.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Resource
    private CompanyService commpanyService;

    @Resource
    private EmployeeService employeeService;

    @GetMapping
    public List<Company> getAllCompanies() {
        return commpanyService.findAll();
    }

    @GetMapping(value = "/{id}")
    public Company getCompanyById(@PathVariable long id) {
        return commpanyService.getCompanyById(id);
    }

    @GetMapping(value = "/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable long id) {
        return employeeService.getEmployeesByCompanyId(id);
    }


    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPageAndSize(@RequestParam int page, @RequestParam int size) {
        return commpanyService.findCompaniesByPageAndSize(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company saveCompany(@RequestBody Company company) {
        return commpanyService.save(company);
    }

    @PutMapping(value = "/{id}")
    public Company updateCompany(@PathVariable long id, @RequestBody Company company) {
        return commpanyService.update(id, company);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable long id) {
        commpanyService.delete(id);
        employeeService.deleteEmployeeByCompanyId(id);
    }

}
