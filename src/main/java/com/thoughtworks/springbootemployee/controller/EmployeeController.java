package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entities.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Resource
    private EmployeeService employeeService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeService.create(employee);
    }
    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getAllEmployee();
    }
    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable("id") Long id) {
        return employeeService.getEmployeeById(id);
    }
    @GetMapping(params = {"gender"})
    public List<Employee> findEmployees(@RequestParam String gender) {
        return employeeService.getEmployeesByGender(gender);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee updateEmployee(@PathVariable("id") Long id, @RequestBody Employee employee) {
        return employeeService.update(id, employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable("id") Long id) {
        employeeService.delete(id);
    }
    @GetMapping(params = {"page", "size"})
    public List<Employee> findEmployeesByPageAndSize(@RequestParam int page, @RequestParam int size) {
        if (page <= 0 || size <= 0) return null;
        return employeeService.getEmployeeByPageAndSize(page, size);
    }
}
