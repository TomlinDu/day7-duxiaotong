package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entities.Company;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> findAll() {
        return companyRepository.getCompanies();
    }
    public Company getCompanyById(long id) {
        return companyRepository.getCompanyById(id);
    }

    public List<Company> findCompaniesByPageAndSize(int page, int size) {
        return companyRepository.findCompaniesByPageAndSize(page, size);
    }

    public Company save(Company company) {
        return companyRepository.save(company);
    }

    public Company update(long id, Company company) {
        return companyRepository.update(id, company);
    }

    public void delete(long id) {
        companyRepository.delete(id);
    }

}
