package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entities.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    public static final String CANNOT_UPDATE_EMPLOYEE = "cannot update employee";
    private final EmployeeRepository employeeRepository;
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }
    public Employee create(Employee employee) {

        return employeeRepository.addEmployee(employee);
    }
    public void delete(Long id) {
        Employee employee = employeeRepository.findById(id);
        employee.setStatus(false);
    }
    public Employee update(Long id, Employee employeeMessage) {
            return employeeRepository.update(id, employeeMessage);
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getEmployeeByPageAndSize(int page, int size) {
        return employeeRepository.findEmployeeByPageAndSize(page, size);
    }
    public List<Employee> getEmployeesByCompanyId(long id) {
        return employeeRepository.getEmployeesByCompanyId(id);
    }
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }
    public List<Employee> getEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public void deleteEmployeeByCompanyId(long id) {
        employeeRepository.updateEmployeeStatusByCompanyId(id);
    }


}
